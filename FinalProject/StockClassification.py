import xlrd
import glob
import xlwt
from Classification import Classi
Farming = [1, 2, 3]
Mining = [5, 6, 7, 8]
Manufacturing = []
for i in range(10,34):
    Manufacturing.append(i)
Electricity = [35, 36]
Waste = [37, 38, 39]
Construction = [41, 42]
Wholesale = [45, 46, 47]
Transportation = [49, 50, 51, 52]
Accommodation = [55, 56]
Publication = [58, 59, 60, 61, 62, 63]
Finance = [64, 65, 66]
Realestate=[68, 69]
Science=[70, 71, 72, 73]
BusinessFacilities=[74, 75]
PublicAdministration=[84]
Education=[85]
Health=[86, 87]
Sports=[90, 91]
Association=[94, 95, 96]
SelfConsumption=[97, 98]
International=[99]
File_List = glob.glob('*).xls')
wbk = xlwt.Workbook()
sheet = wbk.add_sheet('sheet 1')
sheet.write(0, 0, '기업명')
sheet.write(0, 1, '표준산업분류코드')
cnt = 1
for filelist in File_List:
    wb = xlrd.open_workbook(filelist)
    ws = wb.sheet_by_index(0)
    ncol = ws.ncols
    nlow = ws.nrows
    print(filelist)
    stockname = filelist.replace('.xls','')
    stockname = stockname.replace('(2015)','')
    stockname = stockname.replace('(2014)','')
    stockname = stockname.replace('(2013)','')
    stockname = stockname.replace('(2012)','')
    sheet.write(cnt, 0, stockname)

    if (ws.row_values(1)[0] == '문서정보'):
        i = 0
        j = 0
        while i < nlow:

            if("표준산업분류코드" in ws.row_values(i)[0]):
                code = (ws.row_values(i)[0]).split()[2].strip()
                print(code)
                Classi(code,sheet,cnt)
                sheet.write(cnt, 1, code)

            i += 1
    if (ws.row_values(3)[0] == '기 본 정 보'):
        i = 0
        while i < nlow:
            if (ws.row_values(i)[0].strip() == '표준산업분류코드'):
                code = (ws.row_values(i)[1].strip())
                Classi(code, sheet, cnt)
                print(code)
                sheet.write(cnt, 1, code)
            i += 1
    cnt = cnt+1

wbk.save('test.xls')
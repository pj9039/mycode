import numpy as np
import pandas as pd
from scipy.stats.stats import pearsonr
import matplotlib.pyplot as plt
import networkx as nx
import matplotlib
from dsciencelib.servers.hive import HiveServer
from pprint import pprint
import re
from scipy.stats.stats import pearsonr
from datetime import datetime
from pylab import plot,show
from numpy import vstack,array
from numpy.random import rand
from scipy.cluster.vq import kmeans,vq
import numpy as np
import pandas as pd
from scipy.stats.stats import pearsonr
import matplotlib.pyplot as plt
import networkx as nx
import matplotlib
from dsciencelib.servers.hive import HiveServer
from pprint import pprint

'''
원하는 기간과 사이즈를 선택하면 상관계수를 구하는 코드
+ 클러스터링
깃
'''



df_list = []
path = '001.csv'
df= pd.read_csv(path, na_values=['NULL'])
df = df.sort_index(by='date', ascending=True)
df['date'] = pd.to_datetime(df['date'])
df['year'], df['month'] = df['date'].dt.year, df['date'].dt.month
df = df[['date','close','year','month']]
df = df.set_index('date')

'''
startdate1 = input("기준 시작날짜: ")
finishdate1 = input("기준 종료날짜: ")
startdate2 = input("대상 시작날짜: ")
finishdate2 = input("대상 종료날짜: ")
windowsize = int(input("윈도우 사이즈: "))
startdate1=20100101
finishdate1=20101231
startdate2=20120101
finishdate2=20121231
windowsize = 30
'''

windowsize = 60
#df['date'].apply(lambda x: x.strftime('%Y%m%d'))
a = df.loc['20000101':'20051231'].close.values.tolist()
test1 = df.loc['20000101':'20051231'].index.values.astype('M8[D]').astype('O').tolist()
b = df.loc['20060101':'20111231'].close.values.tolist()
test2 = df.loc['20060101':'20111231'].index.values.astype('M8[D]').astype('O').tolist()

while True :
    if(len(a)>len(b)):
        test1.pop()
        a.pop()
    if(len(a)<len(b)):
        test2.pop()
        b.pop()
    if len(a)==len(b) :
        break
while True :
    if(divmod(len(a),windowsize)[1]!=0):
        test1.pop()
        a.pop()
    if (divmod(len(a),windowsize)[1]==0) :
        break
while True :
    if(divmod(len(b),windowsize)[1]!=0):
        test2.pop()
        b.pop()
    if (divmod(len(b),windowsize)[1]==0) :
        break

#여기 위까지 OK

reshpea = list(zip(*[iter(a)]*windowsize))
reshpeb = list(zip(*[iter(b)]*windowsize))
reshpetest1 = list(zip(*[iter(test1)]*windowsize))
reshpetest2 = list(zip(*[iter(test2)]*windowsize))
size = len(reshpea)-1

firstlist = []
finallist = []
firstlist1 = []
finallist1 = []
listmax = []
print('계산')
for i in range(0,size):
    for j in range(0,size):
        #print(reshpetest1[i][0],reshpetest1[i][windowsize-1])
        #print(reshpetest2[j][0],reshpetest2[j][windowsize-1])
        #print(pearsonr(reshpea[i],reshpeb[j])[0])
        firstlist.append(reshpetest1[i][0])
        finallist.append(reshpetest2[j][0])
        firstlist1.append(reshpetest1[i][windowsize-1])
        finallist1.append(reshpetest2[j][windowsize-1])
        listmax.append(pearsonr(reshpea[i],reshpeb[j])[0])

m = max(listmax)

position = [i for i, j in enumerate(listmax) if j == m][0]

print('최고값')
print(firstlist[position])
print(firstlist1[position])
print(finallist[position])
print(finallist1[position])
print(max(listmax))
# data generation

G=nx.Graph()


for i in range(0,len(firstlist)-1):
    G.add_edge('{}'.format(firstlist[i]),'{}'.format(finallist[i]),weight='{}'.format(((listmax[i]))*100))

pos=nx.spring_layout(G) # positions for all nodes
nx.draw_networkx_nodes(G,pos,node_size=600)

# edges


matplotlib.rc('font', family='HYsanB')
# labels
nx.draw_networkx_labels(G,pos,font_size=15,font_family='HYsanB')



plt.axis('off')



plt.show()











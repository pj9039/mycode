import numpy as np
import pandas as pd
from scipy.stats.stats import pearsonr
import matplotlib.pyplot as plt
import networkx as nx
import matplotlib
from dsciencelib.servers.hive import HiveServer
from pprint import pprint
import re
from scipy.stats.stats import pearsonr
from datetime import datetime

'''
원하는 기간과 사이즈를 선택하면 상관계수를 구하는 코드
깃
'''

df_list = [] # csv파일을 넣을 리시트 변수
firstlist = [] # 기준 날짜의 첫날
firstlist1 = [] # 기준 날짜의 마지막날
finallist = [] # 비교 날짜의 첫날
finallist1 = [] # 비교 날짜의 마지막날
listmax = [] # 계산한 피어슨계수를 넣을 리스트 변수

path = '001.csv'
df= pd.read_csv(path, na_values=['NULL'])
df = df.sort_index(by='date', ascending=True) # 날짜를 기준으로 정렬
df['date'] = pd.to_datetime(df['date']) # date를 datetime형식으로 변경
df['year'], df['month'] = df['date'].dt.year, df['date'].dt.month # 년,월을 추출하여 cloumn 생성
df = df[['date','close','year','month']] # 사용할 cloumn 정하기 사실 date랑 close밖에 안쓰지만 나머지는 혹시 몰라서
df = df.set_index('date') # 날짜를 DataFrame의 Index로 설정


startdate1 = input("기준 시작날짜: ")
finishdate1 = input("기준 종료날짜: ")
startdate2 = input("대상 시작날짜: ")
finishdate2 = input("대상 종료날짜: ")
windowsize = int(input("윈도우 사이즈: "))


a = df.loc[startdate1:finishdate1].close.values.tolist() #시작날짜와 종료날짜를 가지고 종가를 추출한뒤 리스트에 입력(기준)
test1 = df.loc[startdate1:finishdate1].index.values.astype('M8[D]').astype('O').tolist() #시작날짜와 종료날짜를 가지고 Index(날짜)를 리스트에 입력(기준)
b = df.loc[startdate2:finishdate2].close.values.tolist() #시작날짜와 종료날짜를 가지고 종가를 추출한뒤 리스트에 입력(비교)
test2 = df.loc[startdate2:finishdate2].index.values.astype('M8[D]').astype('O').tolist()  #시작날짜와 종료날짜를 가지고 Index(날짜)를 리스트에 입력(비교)

# pearson계수를 구하려면 2개의 벡터의 사이즈가 일치해야되는데 일치하지 않을경우 더 큰 리스트의 값을 pop하여 삭제
while True :
    if(len(a)>len(b)):
        test1.pop()
        a.pop()
    if(len(a)<len(b)):
        test2.pop()
        b.pop()
    if len(a)==len(b) :
        break

#리스트를 윈도우사이즈크기로 나눠야 되는데 딱 안떨어지면 나머지가 남기 때문에 정확히 나누기 위한 작업
while True :
    if(divmod(len(a),windowsize)[1]!=0):
        test1.pop()
        a.pop()
    if (divmod(len(a),windowsize)[1]==0) :
        break
while True :
    if(divmod(len(b),windowsize)[1]!=0):
        test2.pop()
        b.pop()
    if (divmod(len(b),windowsize)[1]==0) :
        break

# 정한 윈도우사이즈를 가지고 리스트를 나눔
reshpea = list(zip(*[iter(a)]*windowsize)) # 기준 지수(가격)
reshpeb = list(zip(*[iter(b)]*windowsize)) # 비교 지수(가격)
reshpetest1 = list(zip(*[iter(test1)]*windowsize)) # 기준 지수(가격)의 날짜
reshpetest2 = list(zip(*[iter(test2)]*windowsize)) # 비교 지수(가격)의 날짜
size = len(reshpea)-1


print('계산')
for i in range(0,size):
    for j in range(0,size):
        firstlist.append(reshpetest1[i][0]) # 기준날짜의 첫날짜
        finallist.append(reshpetest2[j][0]) # 비교날짜의 첫날짜
        firstlist1.append(reshpetest1[i][windowsize-1]) # 기준날짜의 마지막날짜
        finallist1.append(reshpetest2[j][windowsize-1]) #
        listmax.append(pearsonr(reshpea[i],reshpeb[j])[0]) # 피어슨 상관계수를 이용 계산후 리스트 변수에 입력

m = max(listmax)

position = [i for i, j in enumerate(listmax) if j == m][0]

print('최고값')
print(firstlist[position])
print(firstlist1[position])
print(finallist[position])
print(finallist1[position])
print(max(listmax))

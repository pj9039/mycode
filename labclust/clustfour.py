import pandas as pd
from scipy.stats.stats import pearsonr
import pymysql
'''
원하는 기간과 사이즈를 선택하면 상관계수를 구하는 코드
+ mysql에 입력
'''

df_list = [] # csv파일을 넣을 리시트 변수
stdstartdate = [] # 기준 날짜의 첫날
stdfinishdate = [] # 기준 날짜의 마지막날
cmpstartdate = [] # 비교 날짜의 첫날
cmpfinishdate = [] # 비교 날짜의 마지막날
pearsonlist = [] # 계산한 피어슨계수를 넣을 리스트 변수

path = '001.csv'
df= pd.read_csv(path, na_values=['NULL'])
df = df.sort_index(by='date', ascending=True) # 날짜를 기준으로 정렬
df['date'] = pd.to_datetime(df['date']) # date를 datetime형식으로 변경
df['year'], df['month'] = df['date'].dt.year, df['date'].dt.month # 년,월을 추출하여 cloumn 생성
df = df[['date','close','year','month']] # 사용할 cloumn 정하기 사실 date랑 close밖에 안쓰지만 나머지는 혹시 몰라서
df = df.set_index('date') # 날짜를 DataFrame의 Index로 설정


stdstartdateinput = input("기준 시작날짜: ")
stdfinishdateinput = input("기준 종료날짜: ")
cmpstartdateinput = input("대상 시작날짜: ")
cmpfinishdateinput = input("대상 종료날짜: ")
windowsize = int(input("윈도우 사이즈: "))


stdpoints = df.loc[stdstartdateinput:stdfinishdateinput].close.values.tolist() #시작날짜와 종료날짜를 가지고 종가를 추출한뒤 리스트에 입력(기준)
stddate = df.loc[stdstartdateinput:stdfinishdateinput].index.values.astype('M8[D]').astype('O').tolist() #시작날짜와 종료날짜를 가지고 Index(날짜)를 리스트에 입력(기준)
cmppoints = df.loc[cmpstartdateinput:cmpfinishdateinput].close.values.tolist() #시작날짜와 종료날짜를 가지고 종가를 추출한뒤 리스트에 입력(비교)
cmpdate = df.loc[cmpstartdateinput:cmpfinishdateinput].index.values.astype('M8[D]').astype('O').tolist()  #시작날짜와 종료날짜를 가지고 Index(날짜)를 리스트에 입력(비교)

# pearson계수를 구하려면 2개의 벡터의 사이즈가 일치해야되는데 일치하지 않을경우 더 큰 리스트의 값을 pop하여 삭제
while True :
    if(len(stdpoints)>len(cmppoints)):
        stddate.pop()
        stdpoints.pop()
    if(len(stdpoints)<len(cmppoints)):
        cmpdate.pop()
        cmppoints.pop()
    if len(stdpoints)==len(cmppoints) :
        break

#리스트를 윈도우사이즈크기로 나눠야 되는데 딱 안떨어지면 나머지가 남기 때문에 정확히 나누기 위한 작업
while True :
    if(divmod(len(stdpoints),windowsize)[1]!=0):
        stddate.pop()
        stdpoints.pop()
    if (divmod(len(stdpoints),windowsize)[1]==0) :
        break
while True :
    if(divmod(len(cmppoints),windowsize)[1]!=0):
        cmpdate.pop()
        cmppoints.pop()
    if (divmod(len(cmppoints),windowsize)[1]==0) :
        break

# 정한 윈도우사이즈를 가지고 리스트를 나눔
reshapestdpoints = list(zip(*[iter(stdpoints)]*windowsize)) # 기준 지수(가격)
reshapecmppoints = list(zip(*[iter(cmppoints)]*windowsize)) # 비교 지수(가격)
reshapestddate = list(zip(*[iter(stddate)]*windowsize)) # 기준 지수(가격)의 날짜
reshapecmpdate = list(zip(*[iter(cmpdate)]*windowsize)) # 비교 지수(가격)의 날짜
size = len(reshapestdpoints)


print('계산')


for i in range(0,size):
    for j in range(0,size):
        stdstartdate.append(reshapestddate[i][0]) # 기준날짜의 첫날짜
        cmpstartdate.append(reshapecmpdate[j][0]) # 비교날짜의 첫날짜
        stdfinishdate.append(reshapestddate[i][windowsize-1]) # 기준날짜의 마지막날짜
        cmpfinishdate.append(reshapecmpdate[j][windowsize-1]) #
        pearsonlist.append(pearsonr(reshapestdpoints[i],reshapecmppoints[j])[0]) # 피어슨 상관계수를 이용 계산후 리스트 변수에 입력

m = max(pearsonlist)

position = [i for i, j in enumerate(pearsonlist) if j == m][0]

print('최고값')
print(stdstartdate[position])
print(stdfinishdate[position])
print(cmpstartdate[position])
print(cmpfinishdate[position])
print(max(pearsonlist))

conn = pymysql.connect(host='210.93.61.152', port=9039, user='root', passwd='rla123', db='clustering',autocommit=True, charset='utf8') # mysql서버 연결(학교서버)

cur = conn.cursor()
#새로 삽입되는 데이터와의 idx를 맞춰주기위해 기존의 idx불러오기
cur.execute("SELECT max(idx) FROM pearson")
for row in cur:
    if row[0]==None:
        lastidx=0
    else:
        lastidx= row[0]

#기준날짜의 시작, 비교날짜의 시작, 피어슨계수, 윈도우 사이즈
for i in range(0,len(stdstartdate)):
    cur.execute("Insert into pearson (stddate,cmpdate, pearson, size) "
            "Values (%s, %s, %s, %s);",[str(stdstartdate[i]).encode('utf-8'), str(cmpstartdate[i]).encode('utf-8'), str(pearsonlist[i]).encode('utf-8'), str(windowsize).encode('utf-8')])


#idx(외래키), 기준날짜, 비교날짜, 기준날짜의 지수값, 비교날짜의 지수값
for i in range(0,len(reshapestddate)):
    for j in range(0,len(reshapecmpdate)):
        for k in range(0,windowsize):
            cur.execute("Insert into points (idx,stddate,cmpdate,stddatepoints, cmpdatepoints) "
                        "Values (%s, %s, %s, %s, %s);",[str(   lastidx+(((i+1)-1)*len(reshapecmpdate))+(j+1)   ).encode('utf-8'), str(reshapestddate[i][k]).encode('utf-8'), str(reshapecmpdate[j][k]).encode('utf-8'), str(reshapestdpoints[i][k]).encode('utf-8'), str(reshapecmppoints[j][k]).encode('utf-8')])
cur.close()





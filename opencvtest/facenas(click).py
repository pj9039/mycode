from threading import Condition
import pygame
import pygame.camera
import http.client
import threading
import click
import pymysql
import time
import os

#로컬에 있는 파일을 MsOxfor Emotion API를 이용하여서 분석하는 함수
def emotion(file):
    list = []
    body = ""
    filename = r'C:/\Users\김민식\PycharmProject\opencvtest\%s'%file
    f = open(filename, "rb")
    body = f.read()
    f.close()
    headers = {
        'Content-Type': 'application/octet-stream',
        'Ocp-Apim-Subscription-Key': '7f8abd9ea32d47ffbad3e0330494d384',
    }
    try:
        conn = http.client.HTTPSConnection('api.projectoxford.ai')
        conn.request("POST", "/emotion/v1.0/recognize", body , headers)
        response = conn.getresponse()#
        #분석결과가 JSON 형식으로 넘어오기 때문에 list형으로 바꾸기 위한 작업
        data = response.read()
        data = data.decode('utf-8').split(":")
        data = str(data).replace("'","")
        data = str(data).replace("{","")
        data = str(data).replace("}","")
        data = str(data).replace("]","")
        data = str(data).replace("[","")
        data = str(data).replace("\"","")
        data = str(data).replace("faceRectangle","")
        data = str(data).replace("height","")
        data = str(data).replace("left","")
        data = str(data).replace("top","")
        data = str(data).replace("width","")
        data = str(data).replace("scores","").strip()
        data = str(data).replace(" ","")
        data = str(data).split(",")
        people = int(len(data)/26)
        for i in range(0,people):
            list.extend([str(i+1), str(format(float(data[26*i+11].strip()),'f')), str(format(float(data[26*i+13].strip()),'f')), format(float(data[26*i+15].strip()),'f'), format(float(data[26*i+17].strip()),'f'), format(float(data[26*i+19].strip()),'f'), format(float(data[26*i+21].strip()),'f'), format(float(data[26*i+23].strip()),'f'),format(float(data[26*i+25].strip()),'f')])
        conn.close()
    except Exception as e:
        print("[Errno {0}] {1}".format(e.errno, e.strerror))
    return list

conn = pymysql.connect(host='pj9039.ipdisk.co.kr', port=3306, user='pj9039', passwd='', db='login',autocommit=True)
current_milli_time = lambda: int(round(time.time() * 1000))

mypicturecnt=0
var = 1
a = current_milli_time()
list = []
queue = [] # 생산자 소비자방식을 이용하기 위한 큐 로컬에 파일을 생성한뒤 분석한다.
mypicturecnt=0
condition = Condition()
def cam():
    global queue
    pygame.camera.init()
    pygame.init()
    cam = pygame.camera.Camera(0,(640,480))
    cam.start()
    global mypicturecnt
    global a,b,c,d
    global snaptime
    while True:
        b = current_milli_time()
        #2초간격으로 사진을 찍는다.
        if (((b-a)%2000)==0 or ((b-a)%2000)==1 or ((b-a)%2000)==2 or ((b-a)%2000)==3or ((b-a)%2000)==4 or ((b-a)%2000)==5 or ((b-a)%2000)==6 or ((b-a)%2000)==7 or ((b-a)%2000)==8 or ((b-a)%2000)==9):
            condition.acquire()
            snaptime=b-a
            img = cam.get_image()
            pygame.image.save(img,"filename%d.jpg"%mypicturecnt) # 사진 저장
            c = "filename%d.jpg"%mypicturecnt
            queue.append(mypicturecnt)
            condition.notify()
            condition.release()
            mypicturecnt += 1

# 분석한 결과를 DB에 저장시키는 함수
@click.command()
@click.option('--name', default='test1', help='insert video name')
def emotionfunc(name):
    global queue
    global k
    k = 0
    while True:
        condition.acquire()
        if not queue:
            condition.wait()
        cur = conn.cursor()
        value = queue.pop(0)
        list = emotion("filename%d.jpg"%value)
        peoplecount = int(len(list)/9)
        print(list)
        #분석값을 넣고 많약에 분석에 실패하면 0값을 넣는다.
        try:
            for people in range(0,peoplecount):
                cur.execute("Insert into emotion (uid, file_name, snap_time, video_name, angry, contempt, disgust, fear, happiness, neutral, sadness, surprise) "
                            "Values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);",[str(list[9*(people)]),str(c),str((snaptime)),str(name),str(list[9*(people)+1]),str(list[9*(people)+2]),str(list[9*(people)+3]),str(list[9*(people)+4]),str(list[9*(people)+5]),str(list[9*(people)+6]),str(list[9*(people)+7]),str(list[9*(people)+8])])
        except:
            for people in range(0,peoplecount):
                cur.execute("Insert into emotion (uid, file_name, snap_time, video_name, angry, contempt, disgust, fear, happiness, neutral, sadness, surprise) "
                            "Values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);",[str(list[9*(people)]),str(c),str((snaptime)),str(name),str(0),str(0),str(0),str(0),str(0),str(0),str(0),str(0)])
        os.remove("%s"%c) # 분석완료한 파일 삭제
        cur.close()
        condition.release()

def main():
    t1=threading.Thread(target=cam)
    t2=threading.Thread(target=emotionfunc)
    t1.start()
    t2.start()

if __name__ == '__main__':
    main()




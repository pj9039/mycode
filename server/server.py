﻿from socket import *
import sys
address = ('127.0.0.1', 65000)
server_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
server_socket.bind(address)

peers = {}

addr1_list = []
addr2_list = []

count = 0
temp1 = 0

while (1):

    # addr[0] = IP 주소
    # addr[1] = PORT 번호
    recv_data, addr = server_socket.recvfrom(500)

    if len(addr1_list) == 0:
        addr1_list.append(addr[0])
        addr1_list.append(addr[1])
    elif (addr1_list[0], addr1_list[1]) != addr and len(addr2_list) == 0:
        addr2_list.append(addr[0])
        addr2_list.append(addr[1])

    print("1번 클라이언트: ", addr1_list, "2번 클라이언트: ", addr2_list)

    data = str(recv_data)
    data = data.replace("b", "")
    data = data.replace("\"", "")
    data = data.replace("\'", "")
    data = data.replace("[", "")
    data = data.replace("]", "")
    data = data.replace(",", "")
    data = data.split()
    data = list(map(int, data))

    print(addr, " 에서 받은데이터: ", data)

    if addr == (addr1_list[0], addr1_list[1]):
        if len(addr2_list) == 0:
            server_socket.sendto("self".encode('utf-8'), (addr1_list[0], addr1_list[1]))
        if len(addr2_list) != 0:
            server_socket.sendto(str(data).encode('utf-8'), (addr2_list[0], addr2_list[1]))
    elif addr == (addr2_list[0], addr2_list[1]):
        server_socket.sendto(str(data).encode('utf-8'), (addr1_list[0], addr1_list[1]))

    # 게임의 승패가 결정되면 주소 초기화
    if data[-1] == 10 or data[-2] == 0:
        addr1_list.clear()
        addr2_list.clear()

   

server_socket.close()

sys.exit(0)
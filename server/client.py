from socket import *
import pygame, random
from pygame.locals import *

#메뉴화면을 위한 함수(외국 홈페이지 참고)
def dumbmenu(screen, menu, x_pos=100, y_pos=100, font=None,
             size=70, distance=1.4, fgcolor=(255, 255, 255),
             cursorcolor=(255, 0, 0), exitAllowed=True):

    pygame.font.init()
    if font == None:
        myfont = pygame.font.Font(None, size)
    else:
        myfont = pygame.font.SysFont(font, size)
    cursorpos = 0
    renderWithChars = False
    for i in menu:
        if renderWithChars == False:
            text = myfont.render(str(cursorpos + 1) + ".  " + i,
                                 True, fgcolor)
        else:
            text = myfont.render(chr(char) + ".  " + i,
                                 True, fgcolor)
            char += 1
        textrect = text.get_rect()
        textrect = textrect.move(x_pos,
                                 (size // distance * cursorpos) + y_pos)
        screen.blit(text, textrect)
        pygame.display.update(textrect)
        cursorpos += 1
        if cursorpos == 9:
            renderWithChars = True
            char = 65

    # Draw the ">", the Cursor
    cursorpos = 0
    cursor = myfont.render(">", True, cursorcolor)
    cursorrect = cursor.get_rect()
    cursorrect = cursorrect.move(x_pos - (size // distance),
                                 (size // distance * cursorpos) + y_pos)

    # The whole While-loop takes care to show the Cursor, move the
    # Cursor and getting the Keys (1-9 and A-Z) to work...
    ArrowPressed = True
    exitMenu = False
    clock = pygame.time.Clock()
    filler = pygame.Surface.copy(screen)
    fillerrect = filler.get_rect()
    while True:
        clock.tick(30)
        if ArrowPressed == True:
            screen.blit(filler, fillerrect)
            pygame.display.update(cursorrect)
            cursorrect = cursor.get_rect()
            cursorrect = cursorrect.move(x_pos - (size // distance),
                                         (size // distance * cursorpos) + y_pos)
            screen.blit(cursor, cursorrect)
            pygame.display.update(cursorrect)
            ArrowPressed = False
        if exitMenu == True:
            break
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return -1
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE and exitAllowed == True:
                    if cursorpos == len(menu) - 1:
                        exitMenu = True
                    else:
                        cursorpos = len(menu) - 1;
                        ArrowPressed = True

                # This Section is huge and ugly, I know... But I don't
                # know a better method for this^^
                if event.key == pygame.K_1:
                    cursorpos = 0;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_2 and len(menu) >= 2:
                    cursorpos = 1;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_3 and len(menu) >= 3:
                    cursorpos = 2;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_4 and len(menu) >= 4:
                    cursorpos = 3;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_5 and len(menu) >= 5:
                    cursorpos = 4;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_6 and len(menu) >= 6:
                    cursorpos = 5;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_7 and len(menu) >= 7:
                    cursorpos = 6;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_8 and len(menu) >= 8:
                    cursorpos = 7;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_9 and len(menu) >= 9:
                    cursorpos = 8;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_a and len(menu) >= 10:
                    cursorpos = 9;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_b and len(menu) >= 11:
                    cursorpos = 10;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_c and len(menu) >= 12:
                    cursorpos = 11;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_d and len(menu) >= 13:
                    cursorpos = 12;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_e and len(menu) >= 14:
                    cursorpos = 13;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_f and len(menu) >= 15:
                    cursorpos = 14;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_g and len(menu) >= 16:
                    cursorpos = 15;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_h and len(menu) >= 17:
                    cursorpos = 16;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_i and len(menu) >= 18:
                    cursorpos = 17;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_j and len(menu) >= 19:
                    cursorpos = 18;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_k and len(menu) >= 20:
                    cursorpos = 19;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_l and len(menu) >= 21:
                    cursorpos = 20;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_m and len(menu) >= 22:
                    cursorpos = 21;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_n and len(menu) >= 23:
                    cursorpos = 22;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_o and len(menu) >= 24:
                    cursorpos = 23;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_p and len(menu) >= 25:
                    cursorpos = 24;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_q and len(menu) >= 26:
                    cursorpos = 25;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_r and len(menu) >= 27:
                    cursorpos = 26;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_s and len(menu) >= 28:
                    cursorpos = 27;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_t and len(menu) >= 29:
                    cursorpos = 28;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_u and len(menu) >= 30:
                    cursorpos = 29;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_v and len(menu) >= 31:
                    cursorpos = 30;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_w and len(menu) >= 32:
                    cursorpos = 31;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_x and len(menu) >= 33:
                    cursorpos = 32;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_y and len(menu) >= 34:
                    cursorpos = 33;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_z and len(menu) >= 35:
                    cursorpos = 34;
                    ArrowPressed = True;
                    exitMenu = True
                elif event.key == pygame.K_UP:
                    ArrowPressed = True
                    if cursorpos == 0:
                        cursorpos = len(menu) - 1
                    else:
                        cursorpos -= 1
                elif event.key == pygame.K_DOWN:
                    ArrowPressed = True
                    if cursorpos == len(menu) - 1:
                        cursorpos = 0
                    else:
                        cursorpos += 1
                elif event.key == pygame.K_KP_ENTER or \
                                event.key == pygame.K_RETURN:
                    exitMenu = True

    return cursorpos

#충돌체크 함수
def collide(x1, x2, y1, y2, w1, w2, h1, h2):
    """
    충돌하면 True 아니면 False 반환
    """
    if x1 + w1 > x2 and x1 < x2 + w2 and y1 + h1 > y2 and y1 < y2 + h2:
        return True
    else:
        return False

#죽은 다음 일어날 함수
def die(screen, score):
    """
    죽으면 점수 출력
    """
    f = pygame.font.SysFont('Arial', 30)
    t = f.render('You lose!!! Your score was: ' + str(score), True, (0, 0, 0))
    screen.blit(t, (10, 270))
    pygame.display.update()
    pygame.time.wait(3000)

#승리한 다음 일어날 함수
def win(screen, score):
    """
    적 지렁이 죽거나 내 지렁이 10점이면 이김
    """
    f = pygame.font.SysFont('Arial', 30)
    t = f.render('You win!!! Your score was: ' + str(score), True, (0, 0, 0))
    screen.blit(t, (10, 270))
    pygame.display.update()
    pygame.time.wait(3000)

pygame.init()

#색지정
red   = 255,  0,  0
green =   0,255,  0
blue  =   0,  0,255

while(1):
    applepos = (random.randint(0, 590), random.randint(0, 590))
    # 지렁이 초기 x좌표와 y좌표
    xs = [290, 290, 290, 290, 290]
    ys = [290, 270, 250, 230, 210]
    dirs = 0
    score = 0
    isAlive = 1
    s = pygame.display.set_mode((600, 600))
    pygame.display.set_caption('Snake')

    # 사과는 빨간색
    appleimage = pygame.Surface((10, 10))
    appleimage.fill((255, 0, 0))

    # 내 뱀은 초록색
    mySnake = pygame.Surface((20, 20))
    mySnake.fill((0, 255, 0))

    # 적 뱀은 파란색
    enemySnake = pygame.Surface((20, 20))
    enemySnake.fill((0, 0, 255))

    f = pygame.font.SysFont('Arial', 20)
    clock = pygame.time.Clock()
    size = width, height = 600,600
    screen = pygame.display.set_mode(size)
    screen.fill(blue)
    pygame.display.update()
    pygame.key.set_repeat(500,30)

    #메뉴선택
    choose = dumbmenu(screen, [
                            'Start Game',
                            'Options',
                            'Manual',
                            'Show Highscore',
                            'Quit Game'], 230,230,None,32,1.4,green,red)
    if choose == 0:
        try:
            print ("You choose 'Start Game'.")
            SERVER_IP = '127.0.0.1'
            SERVER_PORT = 65000
            address = (SERVER_IP, SERVER_PORT)

            client_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
            client_socket.connect(address)

            while True:
                clock.tick(10)

                """
                Esc 키       : 종료
                위 방향키    : 위쪽으로 방향 전환       (dirs = 2)
                아래 방향키  : 아래쪽으로 방향 전환     (dirs = 0)
                왼 방향키    : 왼쪽으로 방향 전환       (dirs = 3)
                오른 방향키  : 오른쪽으로 방향 전환     (dirs = 1)
                """

                for e in pygame.event.get():
                    if e.type == KEYDOWN:
                        if e.key == K_UP and dirs != 0:
                            dirs = 2
                        elif e.key == K_DOWN and dirs != 2:
                            dirs = 0
                        elif e.key == K_LEFT and dirs != 1:
                            dirs = 3
                        elif e.key == K_RIGHT and dirs != 3:
                            dirs = 1

                i = len(xs) - 1

                # 자기 몸에 부딪히면 패배
                while i >= 2:
                    if collide(xs[0], xs[i], ys[0], ys[i], 20, 20, 20, 20):
                        isAlive = 0
                        die(s, score)
                    i -= 1

                # 사과 먹으면 점수 올라가고 (10개 먹으면 승리)
                if collide(xs[0], applepos[0], ys[0], applepos[1], 20, 10, 20, 10):
                    score += 1
                    xs.append(700)
                    ys.append(700)
                    applepos = (random.randint(0, 590), random.randint(0, 590))

                # 내 뱀이 경계선에 부딪히면 패배
                if xs[0] < 0 or xs[0] > 580 or ys[0] < 0 or ys[0] > 580:
                    isAlive = 0
                    die(s, score)
                # 적 뱀과 충돌시에도 패배
                try:
                    j = len(xlist) - 1
                    while j >= 2:
                        if collide(xs[0], xlist[j], ys[0], ylist[j], 20, 20, 20, 20):
                            isAlive = 0
                            die(s, score)
                        j -= 1
                except:
                    pass

                i = len(xs) - 1

                while i >= 1:
                    xs[i] = xs[i - 1]
                    ys[i] = ys[i - 1]
                    i -= 1

                # 이동
                if dirs == 0:
                    ys[0] += 20
                elif dirs == 1:
                    xs[0] += 20
                elif dirs == 2:
                    ys[0] -= 20
                elif dirs == 3:
                    xs[0] -= 20

                # 배경은 하얀색
                s.fill((255, 255, 255))

                sending_data = []
                xlist = []
                ylist = []
                # 좌표에 따라서 내 지렁이 그린다
                for i in range(0, len(xs)):
                    s.blit(mySnake, (xs[i], ys[i]))

                sending_data.append(len(xs))
                sending_data.append(xs)
                sending_data.append(ys)
                sending_data.append(isAlive)
                sending_data.append(score)

                # 문자열로 바꾸어 utf-8 로 인코딩해서 서버로 보낸다
                sending_data = str(sending_data).encode('utf-8')

                client_socket.send(sending_data)

                # 적 뱀 위치 정보 받기

                receiving_data = client_socket.recv(500)

                if receiving_data.decode('utf-8') != 'self':
                    data = str(receiving_data)
                    data = data.replace("b", "")
                    data = data.replace("\"", "")
                    data = data.replace("\'", "")
                    data = data.replace("[", "")
                    data = data.replace("]", "")
                    data = data.replace(",", "")
                    data = data.split()
                    data = list(map(int, data))

                    lengthOfEnemy = data[0]

                    if data[2 * lengthOfEnemy + 1] == 0:
                        win(s, score)
                        break

                    if data[2 * lengthOfEnemy + 2] == 10:
                        die(s, score)
                        break

                    for i in range(0, lengthOfEnemy):
                        xlist.append(data[i + 1])
                    for i in range(0, lengthOfEnemy):
                        ylist.append(data[i + 1 + lengthOfEnemy])

                    print("적 뱀의 x 좌표: ", xlist)
                    print("적 뱀의 y 좌표: ", ylist)



                    # 좌표에 따라서 적 뱀 그린다
                    for i in range(0, len(xlist)):
                        s.blit(enemySnake, (xlist[i], ylist[i]))

                print("내 뱀의 x 좌표: ", xs)
                print("내 뱀의 y 좌표: ", ys)

                # 사과 그리기
                s.blit(appleimage, applepos);
                t = f.render(str(score), True, (0, 0, 0))
                s.blit(t, (10, 10))
                pygame.display.update()


                if score == 10:
                    win(s, score)
                    break

                if isAlive == 0:
                    break


        except:
            pass
        client_socket.close()
    elif choose == 1:
        print ("You choose 'Options'.")
    elif choose == 2:
        print ("You choose 'Manual'.")
    elif choose == 3:
        print ("You choose 'Show Highscore'.")
    elif choose == 4:
        print ("You choose 'Quit Game'.")
        break


pygame.quit()
exit()
